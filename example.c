#include<stdio.h>
#include"torsend.h"

int main(void) {
	puts("connecting...");
	torsock *ts = NULL;
	ts = ts_init("suckless.org", 80);
	if(ts_connect(ts) != 0)
		return -1;
	if(ts_stablish_socks5(ts) != 0)
		return -1;

	ssize_t n = ts_send(ts, "GET / HTTP/1.1\r\nHost: suckless.org\r\nCache-Control: no-cache\r\n\r\n\r\n");
	printf("%ld bytes sent\n", n);

	size_t len = 0;
	char buff[2049] = {'\0'};
	while((len = ts_recv(ts, buff, 2048)) > 0) {
		printf("%s", buff);
		memset(buff, '\0', 2049);
	}

	return 0;
}
