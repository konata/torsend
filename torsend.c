#include "torsend.h"

// TODO: ERROR CHECKING

// initialize the torsock struct
torsock *ts_init(char *domain, short port) {
	torsock *ts = (torsock*)malloc(sizeof(torsock));
	int sock;
	struct sockaddr_in saddr;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(9050);
	saddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ts->sock = sock;
	ts->saddr = saddr;
	ts->domain = domain;
	ts->port = port;
	
	return ts;
}

// connect to the domain
int ts_connect(torsock *ts) {
	return connect(ts->sock,
			(struct sockaddr*)&ts->saddr,
			sizeof(struct sockaddr_in));
}

// stablish the socks5 proxy
int ts_stablish_socks5(torsock *ts) {
	// first request
	char req1[] = {
		0x05, // SOCKS5
		0x01, // One Auth method
		0x00  // No auth
	};
	send(ts->sock, req1, 3, 0);

	char resp1[2];
	recv(ts->sock, resp1, 2, 0);
	if(resp1[1] != 0x00)
		return -1;

	char domain_len = (char)strlen(ts->domain);
	short net_port = htons(ts->port);

	// prepare the second request
	char tmp_req[] = {
		0x05, // SOCKS5
		0x01, // CONNECT
		0x00, // RESERVED
		0x03, // DOMAIN
	};

	// second request
	char *req2 = (char*)malloc(sizeof(char)*(4+1+domain_len+2));
	memcpy(req2, tmp_req, 4);                    // 5, 1, 0, 3
	memcpy(req2 + 4, &domain_len, 1);            // Domain Length
	memcpy(req2 + 5, ts->domain, domain_len);    // Domain
	memcpy(req2 + 5 + domain_len, &net_port, 2); // Port

	send(ts->sock, (char*)req2, 4 + 1 + domain_len + 2, 0);
	
	free(req2);
	req2 = NULL;

	char resp2[10];
	recv(ts->sock, resp2, 10, 0);
	if(resp2[1] != 0x00)
		return -1;

	return 0;
}

// return the number of bytes sent
ssize_t ts_send(torsock *ts, char *msg) {
	return send(ts->sock, msg, strlen(msg), 0);
}

// return the number of bytes received
ssize_t ts_recv(torsock *ts, char *buff, size_t buff_len) {
	return recv(ts->sock, buff, buff_len, 0);
}

