#ifndef TORSEND_H
#define TORSEND_H

#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>

// structure that abstracts socket info for tor use
typedef struct {
	int sock;
	struct sockaddr_in saddr;
	char *domain;
	short port;
} torsock;

torsock *ts_init(char *domain, short port);
int ts_connect(torsock *ts);
int ts_stablish_socks5(torsock *ts);
ssize_t ts_send(torsock *ts, char *msg);
ssize_t ts_recv(torsock *ts, char *buff, size_t buff_len);
#endif
